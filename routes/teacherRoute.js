var teacherDao = require('../dao/indexDao').teacherDao;
var Constants = require('../utils/Constants');

module.exports = function(app) {

	//发布课程
	app.post('/teacher/courseAdd', function(req, res) {
		var course = new Course();
		course.courseName = req.body.courseName;
		course.courseTime = req.body.courseTime;
		course.credit = req.body.credit;
		course.permitNum = req.body.permitNum;
		course.courseType = req.body.courseType;
		course.courseRoom = req.body.courseRoom;
		course.teacherId = req.session.teacherId;
		course.teacherName = req.session.teacherName;
		course.courseText = req.body.courseText;
		if (!course.teacherId) {
			course.teacherId =2;
		}
		if (!course.teacherName) {
			course.teacherName = 'test_teacher'
		}
		teacherDao.insertCourse(course, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0
			})
		})
	})

	app.post('/teacher/getSingleCourse', function(req, res) {
		var courseId = req.body.courseId		
		teacherDao.getSingleCourse(courseId, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				course:ret
			})
		})
	})

	//编辑课程//已经到了选课日期不允许修改
	app.post('/teacher/courseEdit', function(req, res) {
		if (false) { //超过时间限制
			res.send()
			return;
		}
		var course = new Course();
		course.courseName = req.body.courseName;
		course.courseTime = req.body.courseTime;
		course.credit = req.body.credit;
		course.permitNum = req.body.permitNum;
		course.courseType = req.body.courseType;
		course.courseRoom = req.body.courseRoom;
		course.courseId = req.body.courseId;
		course.teacherId = req.session.teacherId;
		course.teacherName = req.session.teacherName;
		course.courseText = req.body.courseText;
		// course.courseId = 2;
		if (!course.teacherId) {
			course.teacherId = 2;
		}
		if (!course.teacherName) {
			course.teacherName = 'test_teacher2'
		}
		teacherDao.updateCourse(course, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0
			})
		})
	})
	app.post('/teacher/score',function(req,res){
		var courseId = req.body.courseId;
		var score = req.body.score;
	})
	//删除课程
	app.post('/teacher/deleteCourse', function(req, res) {
		var courseId = req.body.courseId;
		teacherDao.deleteCourse(courseId, function(err, ret) {
			if (err) {
				console.log(err)
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0
			})
		})
	})
	app.post('/teacher/getCourseListCount', function(req, res) {
		var key = req.body.key;
		if (!key) {
			key = '';
		}
		var teacherId = 2;
		teacherDao.getCourseListCount(teacherId, key, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				courseListCount: ret
			})
		})
	})
	app.post('/teacher/getCourseList', function(req, res) {
		var teacherId = req.session.userId;
		var currentPage = req.body.currentPage;
		var pageSize = parseInt(req.body.pageSize);
		var current = (parseInt(currentPage) - 1) * pageSize;
		var key = req.body.key;
		if (!key) {
			key = '';
		}
		current = 0;
		pageSize = 14;
		teacherId = 2;
		teacherDao.getCourseList(teacherId, current, pageSize, key, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				courseList: ret
			})
		})
	})

	//查看课程中的学生 带分页和查找
	app.get('/teacher/getStudentList', function(req, res) {
		var currentPage = req.body.currentPage;
		var pageSize = parseInt(req.body.pageSize);
		var current = (parseInt(currentPage) - 1) * pageSize;
		var courseName = req.body.courseName;
		var teacherId = req.session.teacherId
		if (!courseName) {
			courseName = ''
		}
		pageSize = 10;
		teacherId = 1;
		current = 0;
		teacherDao.getStudentList(current, pageSize, teacherId, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				courseList: ret
			})
		})
	})

	//给学生打分
	app.get('/teacher/scoreUpdate', function(req, res) {
		var studentId = req.body.studentId;
		var courseId = req.body.courseId;
		var score = req.body.score;
		var studentId = 1;
		var courseId = 1;
		var score = 100;
		teacherDao.scoreUpdate(studentId, courseId, score, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
			})
		})
	})
}

function Course() {}