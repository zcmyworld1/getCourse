var courseDao = require('../dao/indexDao').courseDao;
var Constants = require('../utils/Constants');
var async = require('async');
module.exports = function(app) {
	app.all('/course/*', function(req, res, next) {
		//用户是否登录
		req.session.userId = 1;
		req.session.userName = '志聪'
		if (req.session.userId) {
			next();
		} else {
			res.send({
				error: Constants.ERROR.USER_NOT_LOGIN
			});
			return;
		}
	});
	app.post('/course/searchCourse', function(req, res) {
			var courseName = req.body.courseName;
			courseDao.searchCourse(courseName, function(err, ret) {
				console.log(ret)
				if (err) {
					res.send({
						error: Constants.ERROR.SYS_ERROR
					})
					return;
				}
				res.send({
					error: 0,
					courseList: ret
				})
			})
		})
		/**
		 * 获取所有课程
		 */
	app.post('/course/getCourseList', function(req, res) {
		var currentPage = req.body.currentPage;
		var pageSize = parseInt(req.body.pageSize);
		var current = (parseInt(currentPage) - 1) * pageSize;
		var courseName = req.body.courseName;
		if (!courseName) {
			courseName = ''
		}
		courseDao.getCourseList(current, pageSize, courseName, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				courseList: ret
			})
		})
	})

	/**
	 * 查看课程总数
	 */
	app.post('/course/getCourseCount', function(req, res) {
		courseDao.getCourseCount(function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				courseCount: ret
			})
		})
	})

	/**
	 * 学生选择课程
	 * 每个学生最多选择3门课程
	 * 所选择的课程时间不能重复
	 */
	app.post('/course/ensureCoures', function(req, res) {
		var userId = req.session.userId;
		var courseArr = req.body.courseArr; //数组是课程Id
		var today = new Date();
		var startDay = Constants.SELECT_COURSE_TIME.START_TIME;
		var endDay = Constants.SELECT_COURSE_TIME.END_TIME;
		//如果在非选课时间
		if (today.getTime() < startDay.getTime() || today.getTime() > endDay.getTime()) {
			res.send({
				error: 1,
				startDay: startDay,
				endDay: endDay
			})
			return;
		}
		courseDao.getCourseByUserId(userId, startDay, endDay, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			for (var i in courseArr) {
				for (var j in ret) {
					if (courseArr[i] == ret[j].courseId) {
						res.send({
							error: 2
						})
						return;
					}
				}
			}
			async.eachSeries(courseArr, function(item, callback) {
				courseDao.getCourseById(item, function(err, ret) {
					if (ret[0].permitNum >= ret[0].haveNum) {
						console.log(ret[0])
						var haveNum = parseInt(ret[0].haveNum) + 1
						courseDao.updateHaveNum(item, haveNum, function(err, ret) {
							courseDao.insertStudentCourse(userId, item, function(err, ret) { //将数据插入数据库
								callback();
							})
						})
					}
				})
			}, function(err) {
				console.log(11)
				if (err) {
					res.send({
						error: Constants.ERROR.SYS_ERROR
					})
					return;
				}
				res.send({
					error: 0
				})
			});
		})
	})

	/**
	 * 获取本次选课的课程
	 * 过去选取的课程显示
	 */
	app.post('/course/getHaveSelectCourse', function(req, res) {
			var userId = 1;
			var startDay = Constants.SELECT_COURSE_TIME.START_TIME;
			var endDay = Constants.SELECT_COURSE_TIME.END_TIME;
			courseDao.getHaveSelectCourse(userId, startDay, endDay, function(err, ret) {
				if (err) {
					res.send({
						error: Constants.ERROR.SYS_ERROR
					})
					return;
				}
				res.send({
					error: 0,
					haveSelectCourse: ret
				})
			})
		})
		/**
		 * 获取所有选过的课程
		 */
	app.post('/course/getAllCourse', function(req, res) {
		var userId = req.session.userId;
		var currentPage = req.body.currentPage;
		var pageSize = parseInt(req.body.pageSize);
		var current = (parseInt(currentPage) - 1) * pageSize;

		courseDao.getAllCourse(userId, current, pageSize, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				haveSelectCourse: ret
			})
		})
	})
	app.post('/course/getAllCourseCount', function(req, res) {
			var userId = 1;
			courseDao.getAllCourseCount(userId, function(err, ret) {
				if (err) {
					res.send({
						error: Constants.ERROR.SYS_ERROR
					})
					return;
				}
				console.log(ret);
				res.send({
					error: 0,
					courseCount: ret
				})
			})
		})
		/**
		 * 退选
		 */
	app.post('/course/quitCourse', function(req, res) {
		var userId = 1;
		var courseId = req.body.courseId;
		var startDay = Constants.SELECT_COURSE_TIME.START_TIME;
		var endDay = Constants.SELECT_COURSE_TIME.END_TIME;
		var today = new Date();
		if (today.getTime() < startDay || today.getTime() > endDay) {
			res.send({
				error: 1
			})
			return;
		}
		courseDao.getCourseById(courseId, function(err, ret) {
			if (ret[0].haveNum > 0) {
				var haveNum = parseInt(ret[0].haveNum) - 1;
				courseDao.updateHaveNum(courseId, haveNum, function(err, ret) {
					courseDao.deleteCourse(userId, courseId, function(err, ret) {
						if (err) {
							res.send({
								error: Constants.ERROR.SYS_ERROR
							})
							return;
						}
						res.send({
							error: 0
						})
					})
				})
			}
		})
	})
}