var userRoute = require('./userRoute');
var courseRoute = require('./courseRoute');
var pageRoute = require('./pageRoute');
var teacherRoute = require('./teacherRoute');
var adminRoute = require('./adminRoute');
var commentRoute = require('./commentRoute');
var feedbackRoute = require('./feedbackRoute');
module.exports = function(app) {
	userRoute(app);
	courseRoute(app);
	pageRoute(app);
	teacherRoute(app);
	adminRoute(app);
	commentRoute(app)
	feedbackRoute(app)
}