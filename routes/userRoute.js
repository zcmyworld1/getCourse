var userDao = require('../dao/indexDao').userDao;
var Constants = require('../utils/Constants');
module.exports = function(app) {
	app.post('/user/login', function(req, res) {
		var userName = req.body.userName;
		var password = req.body.password;
		userDao.selectUserByUserName(userName, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			//账号不存在
			if (ret.length == 0) {
				console.log('账号不存在');
				res.send({
					error: 1
				})
				return;
			}
			//密码错误
			if (password != ret[0].password) {
				console.log("密码错误");
				res.send({
					error: 2
				})
				return;
			}
			req.session.userId = ret[0].userId;
			req.session.userName = ret[0].trueName;
			if (ret[0].role == 1) {
				res.send({
					error: 0,
					role: 1,//student
					trueName: ret[0].trueName
				})
			}
			if (ret[0].role == 2) {
				res.send({
					error: 0,
					role: 2,//teacher
					trueName: ret[0].trueName
				});
			}
			if (ret[0].role == 3) {
				res.send({
					error: 0,
					role: 3,//admin
					trueName: ret[0].trueName
				});
			}
		});
	})
}