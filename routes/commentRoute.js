var commentDao = require('../dao/indexDao').commentDao;
var Constants = require('../utils/Constants');
var zcObj = require('../dao/zcDao').zcObj;
var orm_Comment = {
	table: 'comment',
	key: 'commentId',
	courseId: 'courseId',
	studentId: 'studentId',
	content: 'content',
	userName:'userName',
	courseName:'courseName'
}
var orm_Feedback = {
	table: 'feedback',
	key: 'feedbackId',
	phoneNum: 'phoneNum',
	content: 'content',
	userId:'userId'
}
module.exports = function(app) {
	app.all('/comment/*', function(req, res, next) {
		//用户是否登录
		req.session.userId = 1;
		req.session.userName = "志聪"
		if (req.session.userId) {
			next();
		} else {
			res.send({
				error: Constants.ERROR.USER_NOT_LOGIN
			});
			return;
		}
	});
	app.post('/comment/commentAdd', function(req, res) {
		var comment = new zcObj(orm_Comment);
		comment.studentId = req.session.userId;
		comment.content = req.body.content;
		comment.courseId = req.body.courseId;
		comment.userName = req.session.userName;
		comment.courseName = req.body.courseName;
		comment.save(function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0
			})
		});
	})
	app.post('/comment/getCommentCount',function(req,res){
		commentDao.getCommentCount(function(err,ret){
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				commentCount:ret
			})
		})
	})
	app.post('/comment/getCommentList',function(req,res){
		var courseId = req.body.courseId;
		commentDao.getCommentList(courseId,function(err,ret){
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				commentList:ret
			})
		})
	})
}