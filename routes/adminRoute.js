var adminDao = require('../dao/indexDao').adminDao;
var Constants = require('../utils/Constants');
// setTimeout(function() {
	// console.log(Constants)
// }, 2000);

function Course() {}
module.exports = function(app) {
	app.all('/admin/*', function(req, res, next) {
		//用户是否登录
		req.session.userId = 2;
		req.session.userName = "teacher"
		if (req.session.userId) {
			next();
		} else {
			res.send({
				error: Constants.ERROR.USER_NOT_LOGIN
			});
			return;
		}
	});
	app.post('/admin/getCommentCount', function(req, res) {
		var key = req.body.key;
		adminDao.getCommentCount(key, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				commentCount: ret
			})
		})
	})
	app.post('/admin/getCommentList', function(req, res) {
		var key = req.body.key;
		var currentPage = req.body.currentPage;
		var pageSize = parseInt(req.body.pageSize);
		var current = (parseInt(currentPage) - 1) * pageSize;
		adminDao.getCommentList(key, current, pageSize, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				commentList: ret
			})
		})
	})
	app.post('/admin/delComment', function(req, res) {
		var commentId = req.body.commentId;
		adminDao.delComment(commentId, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0
			})
		})
	})
	app.post('/admin/getCourseCount', function(req, res) {
		var key = req.body.key;
		adminDao.getCourseCount(key, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				courseCount: ret
			})
		})
	})
	app.post('/admin/getCourseList', function(req, res) {
		var key = req.body.key;
		var currentPage = req.body.currentPage;
		var pageSize = parseInt(req.body.pageSize);
		var current = (parseInt(currentPage) - 1) * pageSize;
		adminDao.getCourseList(key, current, pageSize, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				courseList: ret
			})
		})
	})
	app.post('/admin/delCourse', function(req, res) {
		var courseId = req.body.courseId;
		adminDao.delCourse(courseId, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0
			})
		})
	})
	app.post('/admin/courseEdit', function(req, res) {
		var course = new Course();
		course.courseName = req.body.courseName;
		course.courseTime = req.body.courseTime;
		course.credit = req.body.credit;
		course.permitNum = req.body.permitNum;
		course.courseType = req.body.courseType;
		course.courseRoom = req.body.courseRoom;
		course.courseId = req.body.courseId;
		course.courseText = req.body.courseText;
		adminDao.updateCourse(course, function(err, ret) {
			if (err) {
				console.log(err)
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0
			})
		})
	})
	app.post('/admin/getSingleCourse', function(req, res) {
		var courseId = req.body.courseId;
		adminDao.getSingleCourse(courseId, function(err, ret) {
			if (err) {
				console.log(err)
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				course: ret
			})
		})
	})
	app.post('/admin/getStudentCount', function(req, res) {
		var key = req.body.key;
		adminDao.getStudentCount(key, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				studentCount: ret
			})
		})
	})
	app.post('/admin/getStudentList', function(req, res) {
		var key = req.body.key;
		var currentPage = req.body.currentPage;
		var pageSize = parseInt(req.body.pageSize);
		var current = (parseInt(currentPage) - 1) * pageSize;
		adminDao.getStudentList(key, current, pageSize, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				studentList: ret
			})
		})
	})
	app.post('/admin/getStudentListByCourse', function(req, res) {
		var courseId = req.body.courseId;
		adminDao.getStudentListByCourse(courseId, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				studentList: ret
			})
		})
	})
	app.post('/admin/updateTime', function(req, res) {
		function timmer() {}
		timmer.startYear = req.body.startYear;
		timmer.startMonth = req.body.startMonth;
		timmer.startDay = req.body.startDay;
		timmer.endYear = req.body.endYear;
		timmer.endMonth = req.body.endMonth;
		timmer.endDay = req.body.endDay;
		adminDao.updateTime(timmer, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error:0
			})
		})
	})
	app.post('/admin/getTime',function(req,res){
		adminDao.getTime(function(err,ret){
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error:0,
				timmer:ret
			})
		})
	})
}