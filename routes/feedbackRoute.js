var adminDao = require('../dao/indexDao').adminDao;
var Constants = require('../utils/Constants');
var zcObj = require('../dao/zcDao').zcObj;
var orm_Feedback = {
	table: 'feedback',
	key: 'feedbackId',
	phoneNum: 'phoneNum',
	content: 'content',
	userId: 'userId',
	isDue:'isDue',
	userName:'userName'
}
module.exports = function(app) {
	app.all('/feedback/*', function(req, res, next) {
		//用户是否登录
		req.session.userId = 1;
		req.session.userName = '志聪'
		if (req.session.userId) {
			next();
		} else {
			res.send({
				error: Constants.ERROR.USER_NOT_LOGIN
			});
			return;
		}
	});
	app.post('/feedback/feedbackAdd', function(req, res) {
		var feedback = new zcObj(orm_Feedback)
		feedback.userId = req.session.userId;
		feedback.content = req.body.content;
		feedback.phoneNum = req.body.phoneNum;
		feedback.userName = req.session.userName;
		feedback.save(function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0
			})
		})
	})
	app.post('/feedback/getFeedbackCount', function(req, res) {
		var key = req.body.key
		adminDao.getFeedbackCount(key,function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				feedbackCount: ret
			})
		})
	})
	app.post('/feedback/getFeedbackList', function(req, res) {
		var currentPage = req.body.currentPage;
		var pageSize = parseInt(req.body.pageSize);
		var current = (parseInt(currentPage) - 1) * pageSize;
		var key = req.body.key;
		adminDao.getFeedbackList(current, pageSize,key, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				feedbackList:ret
			})
		})
	})
	app.post('/feedback/updateIsDue',function(req,res){
		var feedbackId = req.body.feedbackId;
		var feedback = new zcObj(orm_Feedback)
		feedback.isDue = 2;
		adminDao.updateIsDue(1,feedbackId,function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0
			})
		})
	})
}