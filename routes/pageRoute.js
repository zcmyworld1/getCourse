var Constants = require('../utils/Constants');
module.exports = function(app) {
	app.get('/', function(req, res) {
		res.sendfile('views/index.html');
	})
	app.get('/getScore', function(req, res) {
		res.sendfile('views/getScore.html')
	})
	app.get('/student', function(req, res) {
		res.sendfile('views/student.html');
	})
	app.get('/teacher', function(req, res) {
		res.sendfile('views/teacher.html');
	})
	app.get('/teacher_student',function(req,res){
		res.sendfile('views/teacher_student.html')
	})

	//admin
	app.get('/admin_feedback', function(req, res) {
		res.sendfile('views/admin_feedback.html');
	})
	app.get('/admin_comment', function(req, res) {
		res.sendfile('views/admin_comment.html');
	})
	app.get('/admin', function(req, res) {
		res.sendfile('views/admin.html');
	})
	app.get('/admin_student', function(req, res) {
		res.sendfile('views/admin_student.html');
	})
	app.get('/admin_system', function(req, res) {
		res.sendfile('views/admin_system.html');
	})
}