﻿// var sys_cfg = require('../../readshare/config/config.js');
var mysql = require('mysql')
var pool = null;
function init() {
	if (pool) {
		return;
	}
	return pool = mysql.createPool({
		host: "127.0.0.1",
		port: "3306",
		user: "root",
		password: "root",
		//最多20个并发连接
		connectionLimit: 20,
		charset: 'utf8',
		database: "getcourse"
	});
}
exports.pool = init();