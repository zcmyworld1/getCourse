var courseDao = module.exports;
// var dbclient = require('./mysql/mysql').init(null);
var dbclient = require('./mysql/mysql').pool;
/**
 * 获取所有课程
 */
courseDao.getCourseList = function(current, pageSize, courseName, callback) {
	var sql = 'SELECT *,a.courseId from course AS a LEFT JOIN (SELECT count(courseId) as courseCount,courseId from course where courseName LIKE ?) AS b ON a.courseId=b.courseId WHERE courseName LIKE ? limit ?,?';
	var args = ['%' + courseName + '%', '%' + courseName + '%', current, pageSize];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
	// console.log(1)
}
courseDao.getCourseById = function(courseId,callback){
	var sql = "select * from course where courseId = ?"
	var args = [courseId];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
}
courseDao.updateHaveNum = function(courseId,haveNum,callback){
	var sql = 'UPDATE course SET haveNum=? WHERE courseId = ?'
	var args = [haveNum,courseId];
	dbclient.query(sql, args, function(err, res) {
		console.log(err)
		callback(err, res);
	});
}
/**
 * 根据时间获取某个学生选过的课程
 */
courseDao.getCourseByUserId = function(userId, startTime, endTime, callback) {
		var sql = 'select userId,courseId,studentCourseTime from studentCourse where userId = ? and studentCourseTime > ? and studentCourseTime < ?';
		var args = [userId, startTime, endTime];
		dbclient.query(sql, args, function(err, res) {
			callback(err, res);
		})
	}
	/**
	 * 获取课程数量
	 */
courseDao.getCourseCount = function(callback) {
	var sql = 'select count(courseId) as courseCount from course';
	var args = null;
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
}

/**
 * 学生选课
 */
courseDao.insertStudentCourse = function(userId, courseId, callback) {
	var sql = 'insert into studentCourse (userId, courseId) values (?,?)';
	var args = [userId, courseId];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}

/**
 * 获取已选课程
 */
courseDao.getHaveSelectCourse = function(userId, startTime, endTime, callback) {
		var sql = 'select sc.userId,sc.courseId,sc.studentCourseTime,c.courseName from studentcourse as sc left join course as c on sc.courseId = c.courseId where userId = ? and sc.studentCourseTime > ? and sc.studentCourseTime < ?';
		var args = [userId, startTime, endTime];
		dbclient.query(sql, args, function(err, res) {
			callback(err, res);
		});
	}
	/**
	 * 获取所有课程
	 */
courseDao.getAllCourse = function(userId, current, pageSize, callback) {
	var sql = 'select sc.userId,sc.courseId,sc.score,sc.studentCourseTime,c.courseName,c.courseType,c.teacherName,c.courseTime,c.credit,c.courseRoom from studentcourse as sc left join course as c on sc.courseId = c.courseId where userId =? limit ?,?';
	var args = [userId, current, pageSize];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}

courseDao.getAllCourseCount = function(userId, callback) {
		var sql = 'select count(userId) as courseCount from studentCourse where userId = ?';
		var args = [userId];
		dbclient.query(sql, args, function(err, res) {
			callback(err, res);
		});
	}
	/**
	 * 退选
	 */
courseDao.deleteCourse = function(userId, courseId, callback) {
	var sql = 'delete from studentCourse where userId = ? and courseId = ?';
	var args = [userId, courseId];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}

//查找课程
courseDao.searchCourse = function(courseName, callback) {
	var sql = 'SELECT * from course AS a LEFT JOIN (SELECT count(courseId) as courseCount,courseId from course) AS b ON a.courseId=b.courseId WHERE courseName LIKE ?'
	var args = ['%' + courseName + '%']
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}