var dbclient = require('./mysql/mysql').pool;

function zcObj(config) {
	this.config = config
}
var zcMethod = {
	config: 1,
	save: 1,
	update: 1,
	delete: 1
}
zcObj.prototype.save = function(callback) {
	var obj = this
	var config = this.config
	var column_key = "";
	var column_val = "";
	var args = [];
	for (var key in obj) {
		if (zcMethod[key]) {
			continue;
		}
		column_key += config[key] + ','
		column_val += "?,"
		args.push(obj[key])
	}
	column_key = column_key.slice(0, column_key.length - 1)
	column_val = column_val.slice(0, column_val.length - 1)
	var sql = "INSERT INTO " + config.table + "(" + column_key + ")" + "VALUE(" + column_val + ")";
	console.log(sql)
	execQuery(sql, args, callback)
}

zcObj.prototype.delete = function(id,callback) {
	var config = this.config
	var sql = "delete from "+config.table +" where "+config.key +" = ?"
	var args = [id]
	execQuery(sql,args,callback)
}

zcObj.prototype.update = function(callback){
	var obj = this
	var config = this.config
	var column_key = "";
	var column_val = "";
	var args = [];
	for (var key in obj) {
		if (zcMethod[key]) {
			continue;
		}
		column_key += config[key] + "= ?,"
		args.push(obj[key])
	}
	column_key = column_key.slice(0, column_key.length - 1)
	var sql = "UPDATE " + config.table + " SET "+column_key+" where key=?"
	execQuery(sql, args, callback)
}

function execQuery(sql, args, callback) {
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
}

exports.zcObj = zcObj;

