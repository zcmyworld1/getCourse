var adminDao = module.exports;
var dbclient = require('./mysql/mysql').pool;

adminDao.getFeedbackCount = function(key, callback) {
	var sql = "select count(*) as feedbackCount from feedback where userName like ?"
	var args = ['%' + key + '%'];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
}

adminDao.getFeedbackList = function(current, pageSize, key, callback) {
	var sql = 'SELECT * FROM feedback where userName like ? group by feedbackTime desc limit ?,?'
	var args = ['%' + key + '%', current, pageSize]
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
}
adminDao.updateIsDue = function(isDue, feedbackId, callback) {
	var sql = "UPDATE feedback set isDue = ? where feedbackId=?";
	var args = [isDue, feedbackId]
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
}

adminDao.getCommentCount = function(key, callback) {
	var sql = "select count(*) as commentCount from comment where userName like ?";
	var args = ['%' + key + '%']
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
}
adminDao.getCommentList = function(key, current, pageSize, callback) {
	var sql = "select * from comment where userName like ? group by commentTime desc limit ?,?"
	var args = ['%' + key + '%', current, pageSize]
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
}

adminDao.delComment = function(commentId, callback) {
	var sql = "delete from comment where commentId = ?";
	var args = [commentId]
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
}

adminDao.getCourseCount = function(key, callback) {
	var sql = "select count(*) as courseCount from course where courseName like ?";
	var args = ['%' + key + '%']
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
}
adminDao.getCourseList = function(key, current, pageSize, callback) {
	var sql = "select * FROM course where courseName like ? group by insertTime limit ?,?";
	var args = ['%' + key + '%', current, pageSize]
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
}

adminDao.delCourse = function(courseId, callback) {
	var sql = "delete from course where courseId = ?";
	var args = [courseId]
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
}

adminDao.updateCourse = function(course, callback) {
	var sql = 'UPDATE course SET courseText=?,courseName=?,courseTime=?,credit=?,permitNum=?,courseType=?,courseRoom=? WHERE courseId = ?'
	var args = [course.courseText, course.courseName, course.courseTime, course.credit, course.permitNum, course.courseType, course.courseRoom, course.courseId];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}
adminDao.getSingleCourse = function(courseId, callback) {
	var sql = "SELECT * FROM course where courseId = ?"
	var args = [courseId]
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}
adminDao.getStudentCount = function(key, callback) {
	var sql = "select count(*) as studentCount from student where trueName like ?"
	var args = ['%' + key + '%'];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
}

adminDao.getStudentList = function(key, current, pageSize, callback) {
	var sql = "select * FROM student where trueName like ? limit ?,?";
	var args = ['%' + key + '%', current, pageSize]
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
}

adminDao.getStudentListByCourse = function(courseId, callback) {
	var sql = "SELECT * FROM student AS s LEFT JOIN studentcourse AS sc ON s.userId=sc.userId WHERE sc.courseId=?";
	var args = [courseId]
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
}
adminDao.getTime = function(callback){
	var sql = "select * from courseConfig"
	var args = null
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
}
adminDao.updateTime = function(timmer, callback) {
	var sql = "UPDATE courseConfig set startYear = ? ,startMonth = ?,startDay = ?,endYear=?,endMonth=?,endDay=? where id=1";
	var args = [timmer.startYear, timmer.startMonth, timmer.startDay, timmer.endYear, timmer.endMonth, timmer.endDay]
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
}