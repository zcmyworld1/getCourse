var dbclient = require('./mysql/mysql').pool;
var commentDao = module.exports;

commentDao.getCommentCount = function(callback){
	var sql = "select count(*) as commentCount from comment"
	var args = null;
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
}

commentDao.getCommentList = function(courseId,callback){
	var sql = "SELECT * FROM comment AS c LEFT JOIN `user` AS u ON c.studentId = u.userId WHERE c.courseId = ? GROUP BY commentTime DESC"
	var args = [courseId];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})	
}