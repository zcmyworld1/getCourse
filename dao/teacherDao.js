var teacherDao = module.exports;
var dbclient = require('./mysql/mysql').pool;

teacherDao.getSingleCourse = function(courseId,callback){
	var sql = "SELECT * FROM course where courseId = ?"
	var args = [courseId]
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}
teacherDao.insertCourse = function(course, callback) {
	var sql = 'insert into course (courseName, teacherId,teacherName,courseTime,credit,permitNum,courseType,courseRoom,courseText) values (?,?,?,?,?,?,?,?,?)';
	var args = [course.courseName, course.teacherId, course.teacherName, course.courseTime, course.credit, course.permitNum, course.courseType, course.courseRoom,course.courseText];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}

teacherDao.updateCourse = function(course, callback) {
	var sql = 'UPDATE course SET courseText=?,courseName=?,teacherId=?,teacherName=?,courseTime=?,credit=?,permitNum=?,courseType=?,courseRoom=? WHERE courseId = ?'
	var args = [course.courseText,course.courseName, course.teacherId, course.teacherName, course.courseTime, course.credit, course.permitNum, course.courseType, course.courseRoom, course.courseId];
	console.log(sql)
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}
teacherDao.deleteCourse = function(courseId, callback) {
	var sql = 'delete from course where courseId = ?';
	var args = [courseId];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}
teacherDao.getStudentList = function(current, pageSize, teacherId, callback) {
	var sql = 'SELECT * FROM course AS c LEFT JOIN studentcourse AS sc ON c.courseId=sc.courseId WHERE teacherId=? LIMIT ?,?'
	var args = [teacherId, current, pageSize]
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}
teacherDao.scoreUpdate = function(userId, courseId, score, callback) {
	var sql = 'update studentcourse set score = ? where userId =? and courseId=?'
	var args = [score, userId, courseId];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}
teacherDao.getCourseListCount = function(teacherId,key, callback) {
	var sql = 'SELECT COUNT(*) AS courseCount,courseId FROM course WHERE teacherId=? and courseName like ?';
	var args = [teacherId,'%'+key+'%']
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}
teacherDao.getCourseList = function(teacherId, current, pageSize, key, callback) {
	var sql = 'SELECT * FROM course AS c WHERE teacherId=? and courseName like ? limit ?,?';
	var args = [teacherId, '%' + key + '%', current, pageSize]
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}