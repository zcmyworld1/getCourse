var dbclient = require('../dao/mysql/mysql').pool;
var sql = "select * from courseConfig"
var args = null;

//选课的开始时间
var startTime = new Date();
var endTime = new Date();

var startYear = ""
var startMonth = ""
var startDay = ""

var endYear = "";
var endMonth = "";
var endDay = ""


dbclient.query(sql, args, function(err, res) {
	startYear = res[0].startYear
	startMonth = res[0].startMonth
	startDay = res[0].startDay

	endYear = res[0].endYear
	endMonth = res[0].endMonth
	endDay = res[0].endDay

	//设置选课时间
	// startTime.setFullYear(2014, (startMonth - 1), 10);
	// endTime.setFullYear(2014, (endMonth - 1), 21);
	startTime.setFullYear(startYear,(startMonth - 1),startDay)
	endTime.setFullYear(endYear,(endMonth - 1),endDay)

	module.exports.SELECT_COURSE_TIME = {
		START_TIME: startTime,
		END_TIME: endTime
	}
})
module.exports = {
	ERROR: {
		SYS_ERROR: 102,
		USER_NOT_LOGIN: 101
	},
	// SELECT_COURSE_TIME: {}
}