/*
Navicat MySQL Data Transfer

Source Server         : root
Source Server Version : 50515
Source Host           : localhost:3306
Source Database       : getcourse

Target Server Type    : MYSQL
Target Server Version : 50515
File Encoding         : 65001

Date: 2015-03-11 23:19:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `comment`
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `commentId` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT '',
  `courseId` int(11) DEFAULT '0',
  `studentId` int(11) DEFAULT '0',
  `commentTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `userName` varchar(11) DEFAULT '',
  `courseName` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`commentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comment
-- ----------------------------

-- ----------------------------
-- Table structure for `course`
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `courseId` int(11) NOT NULL AUTO_INCREMENT,
  `courseName` varchar(30) DEFAULT '',
  `teacherId` int(11) DEFAULT '0',
  `teacherName` varchar(30) DEFAULT '',
  `courseTime` varchar(30) DEFAULT '',
  `credit` int(11) DEFAULT '0',
  `permitNum` int(11) DEFAULT '0',
  `courseType` int(3) DEFAULT '0' COMMENT '1,限定性艺术类，2，非限定性艺术类说',
  `courseRoom` varchar(30) DEFAULT '',
  `courseText` varchar(255) DEFAULT '',
  `insertTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`courseId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course
-- ----------------------------

-- ----------------------------
-- Table structure for `feedback`
-- ----------------------------
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `feedbackId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT '0',
  `userName` varchar(20) DEFAULT '',
  `phoneNum` varchar(11) DEFAULT '',
  `content` varchar(255) DEFAULT '',
  `isDue` int(3) DEFAULT '0' COMMENT '1未处理，2已经处理',
  `feedbackTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`feedbackId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of feedback
-- ----------------------------

-- ----------------------------
-- Table structure for `student`
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `trueName` varchar(22) DEFAULT '',
  `major` varchar(11) DEFAULT '',
  `grade` varchar(11) DEFAULT '',
  `stuNum` varchar(11) DEFAULT '' COMMENT '学号',
  `sex` int(3) DEFAULT '0' COMMENT '1:man2:woman',
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of student
-- ----------------------------

-- ----------------------------
-- Table structure for `studentcourse`
-- ----------------------------
DROP TABLE IF EXISTS `studentcourse`;
CREATE TABLE `studentcourse` (
  `userId` int(11) NOT NULL DEFAULT '0',
  `courseId` int(11) NOT NULL DEFAULT '0',
  `studentCourseTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '学生选课的时间',
  `score` int(11) DEFAULT '0',
  PRIMARY KEY (`userId`,`courseId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of studentcourse
-- ----------------------------

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(30) DEFAULT '',
  `password` varchar(30) DEFAULT '',
  `trueName` varchar(30) DEFAULT '',
  `role` varchar(3) DEFAULT '1' COMMENT '1为学生，2为教师',
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
