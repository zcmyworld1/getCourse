var courseType_g = 0;
$(document).ready(function() {
	var pageCount;
	var currentPage = 1;
	getCourseCount()
	$("#userMangeUserName").click(function() {
		$("#userManageSelect").slideToggle(1)
	})
	$("#logout").click(function() {
		$("#userManageSelect").slideUp(1)
		alert('退出登录')
	})
	$("#searchEnsure").click(function() {
		getCourseCount()
	})
	$("#ct_left").click(function() {
		$("#ct_right_bottom").fadeIn();
		$("#ct_right_top").fadeIn();
	})
	$("#ct_right_bottom").click(function() {
		//非限定性艺术类
		$("#ct_right_bottom").fadeOut();
		$("#ct_right_top").fadeOut();
		$("#ct_left").text($(this).text());
		courseType_g = 2;
	})
	$("#ct_right_top").click(function() {
		//限定性艺术类
		$("#ct_right_bottom").fadeOut();
		$("#ct_right_top").fadeOut();
		$("#ct_left").text($(this).text());
		courseType_g = 1;
	})
	$(document).on('click','.del',function(){
		var courseId = $(this).parent().parent().find('.courseId').val();
		var route = "admin/delCourse";
		var args = {
			courseId:courseId
		}
		ajaxPost(route,args,function(data){
			if(data.error){
				alert('系统错误：'+data.error)
				return;
			}
			alert('删除成功')
			getCourseList(1)
		})
	})
	$(document).on('click', '.edit', function() {
		var courseId = $(this).parent().parent().find('.courseId').val();
		var route = "admin/getSingleCourse";
		var args = {
			courseId: courseId
		}
		ajaxPost(route, args, function(data) {
			$("#addCourseBox").show();
			$("#addCourse_courseId").val(data.course[0].courseId)
			$("#addCourseBox_courseName input").val(data.course[0].courseName);
			$("#addCourseBox_courseTime input").val(data.course[0].courseTime);
			$("#addCourseBox_credit input").val(data.course[0].credit);
			$("#addCourseBox_permitNum input").val(data.course[0].permitNum);
			$("#addCourseBox_courseRoom input").val(data.course[0].courseRoom);
			if (data.course[0].courseType == 1) {
				//限定性艺术类
				$("#ct_left").text("限定性艺术类");
				courseType_g = 1;
			}
			if (data.course[0].courseType == 2) {
				courseType_g = 2;
				$("#ct_left").text("非限定性艺术类");
			}
			$("#addCourseBox_courseText textarea").val(data.course[0].courseText);
		})
	})
	$("#addCourseBox_close").click(function() {
		$("#addCourseBox").hide()
	})
	$("#addCourseBox_ensure").click(function() {
		var courseId = $("#addCourse_courseId").val();
		var courseName = $("#addCourseBox_courseName input").val();
		var courseTime = $("#addCourseBox_courseTime input").val();
		var credit = $("#addCourseBox_credit input").val();
		var permitNum = $("#addCourseBox_permitNum input").val();
		var courseRoom = $("#addCourseBox_courseRoom input").val();
		var courseType = courseType_g;
		var courseText = $("#addCourseBox_courseText textarea").val();
		var args = {
			courseId:courseId,
			courseName: courseName,
			courseTime: courseTime,
			credit: credit,
			permitNum: permitNum,
			courseRoom: courseRoom,
			courseType: courseType,
			courseText: courseText
		}
		var route = "admin/courseEdit";
		ajaxPost(route, args, function(data) {
			alert('编辑成功');
			$("#addCourseBox").hide();
			getCourseList(currentPage)
		})
	})
});

function getCourseCount() {
	var route = "admin/getCourseCount";
	var key = $("#searcheCourse").val()
	var args = {
		key: key
	}
	ajaxPost(route, args, function(data) {
		pageCount = data.courseCount[0].courseCount;
		pageCount = parseInt(data.courseCount[0].courseCount / 14) + 1;
		$("#pageCount").html(pageCount)
		currentPage = parseInt($("#currentPage").html())
		getCourseList(currentPage)
	})
}

function getCourseList(currentPage) {
	$(".course_ele").remove();
	var route = "admin/getCourseList";
	var key = $("#searcheCourse").val()
	var pageSize = 14;
	var args = {
		currentPage: currentPage,
		pageSize: pageSize,
		key: key
	}
	ajaxPost(route, args, function(data) {
		console.log(data.courseList)
		for (var i in data.courseList) {
			var t = data.courseList[i].courseType;
			if (t == '1') {
				t = "限定性艺术类"
			}
			if (t == '2') {
				t = "非限定性艺术类"
			}
			var html = '<tr class="course_ele">' +
				'<input type="hidden" value="' + data.courseList[i].courseId + '" class="courseId">' +
				'<td class="courseName">' + data.courseList[i].courseName + '</td>' +
				'<td class="courseTeacher">' + data.courseList[i].teacherName + '</td>' +
				'<td class="courseTime" style="width:100px;"">' + data.courseList[i].courseTime + '</td>' +
				'<td class="courseCredit">' + data.courseList[i].credit + '</td>' +
				'<td class="courseType" style="width:180px;">' + t + '</td>' +
				'<td class="courseRoom">' + data.courseList[i].courseRoom + '</td>' +
				'<td class="permitNum">' + data.courseList[i].permitNum + '</td>' +
				'<td class="haveNum">' + 1 + '</td>' +
				'<td class="operation"><div class="edit">编辑</div><div class="del">删除</div></td>' +
				'</tr>'
			$("#courseTable").append(html)
		}
	})
}