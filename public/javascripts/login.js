$(document).ready(function() {
	$("#btn_ensure").click(function() {
		var userName = $("#userName").val();
		var password = $("#password").val();
		if (userName.length <= 0) {
			$("#sysHint").html("账号不能为空");
			return;
		}
		if (password.length <= 0) {
			$("#sysHint").html("密码不能为空");
			return;
		}
		var route = "user/login";
		var args = {
			userName: userName,
			password: password
		}
		ajaxPost(route, args, function(data) {
			// alert(data.error);
			if (data.error == 1) {
				$("#sysHint").html("账号不存在");
				return;
			}
			if (data.error == 2) {
				$("#sysHint").html("密码错误");
				return;
			}
			$("#sysHint").html("正在登陆..");
			$.cookie('userName', data.trueName, {
				expires: 3650
			});
			if (data.role == 1) {
				window.location.href = "/student?userName=" + data.trueName;
			}
			if (data.role == 2) {
				window.location.href = '/teacher?userName=' + data.trueName;
			}
			if (data.role == 3) {
				window.location.href = '/admin?adminName=' + data.trueName;
			}
		})
	})
});