$(document).ready(function() {
	getInitCurrentPage();
	getAllCourseCount();
	getAllCourse();
	$(document).on('click', '#prevPage', function() {
		var currentPage = parseInt($("#currentPage").html());
		if (currentPage == 1) {
			alert("已经是首页");
			return;
		}
		var currentPage = parseInt($("#currentPage").html()) - 1;
		$("#currentPage").html(currentPage);
		getAllCourse();
	})
	$(document).on('click', '#nextPage', function() {
		var currentPage = parseInt($("#currentPage").html());
		var pageCount = parseInt($("#pageCount").html());
		if (currentPage == pageCount) {
			alert("已经是最后一页");
			return;
		}
		var currentPage = parseInt($("#currentPage").html()) + 1;
		$("#currentPage").html(currentPage);
		getAllCourse();
	})
});

function getInitCurrentPage() {
	$("#currentPage").html(1);
}

function getAllCourseCount() {
	var route = "course/getAllCourseCount";
	var args = null;
	ajaxPost(route, args, function(data) {
		pageCount = parseInt(data.courseCount[0].courseCount / 14) + 1;
		$("#pageCount").html(pageCount);
	})
}

function getAllCourse() {
	var currentPage = parseInt($("#currentPage").html());
	$(".courseData").remove();
	var route = 'course/getAllCourse';
	var args = {
		currentPage: currentPage,
		pageSize: 14
	};
	ajaxPost(route, args, function(data) {
		for (var i in data.haveSelectCourse) {
			if (data.haveSelectCourse[i].courseType == 1) {
				data.haveSelectCourse[i].courseType = "限定性艺术类";
			}
			if (data.haveSelectCourse[i].courseType == 2) {
				data.haveSelectCourse[i].courseType = "非限定性艺术类";
			}
			data.haveSelectCourse[i].studentCourseTime = new Date(data.haveSelectCourse[i].studentCourseTime).getFullYear() + '/' + (parseInt(new Date(data.haveSelectCourse[i].studentCourseTime).getMonth()) + 1)
			var html =
				"<tr class='courseData'>" +
				"<input type='hidden' class='courseId' value='" + data.haveSelectCourse[i].courseId + "'>" +
				"<td class='courseName'>" + data.haveSelectCourse[i].courseName + "</td>" +
				"<td>" + data.haveSelectCourse[i].teacherName + "</td>" +
				"<td>" + data.haveSelectCourse[i].courseType + "</td>" +
				"<td>" + data.haveSelectCourse[i].studentCourseTime + "</td>" +
				"<td>" + data.haveSelectCourse[i].courseTime + "</td>" +
				"<td>" + data.haveSelectCourse[i].courseRoom + "</td>" +
				"<td>" + data.haveSelectCourse[i].credit + "</td>" +
				"<td>" + data.haveSelectCourse[i].score + "</td>" +
				"</tr>";
			$("#courseTable").append(html);
		}
	})
}