var courseType_g = 0;
$(document).ready(function() {
	$("#userMangeUserName").click(function() {
		$("#userManageSelect").slideToggle(1)
	})
	$("#logout").click(function() {
		$("#userManageSelect").slideUp(1)
		alert('退出登录')
	})
	getTime()
	$("#ensure").click(function(){
		var startYear = $("#startYear").val()
		var startMonth = $("#startMonth").val()
		var startDay = $("#startDay").val()
		var endYear = $("#endYear").val();
		var endMonth = $("#endMonth").val();
		var endDay = $("#endDay").val()
		var route = "admin/updateTime"
		var args = {
			startYear:startYear,
			startMonth:startMonth,
			startDay:startDay,
			endYear:endYear,
			endMonth:endMonth,
			endDay:endDay
		}
		ajaxPost(route,args,function(data){
			if(data.error){
				alert('系统错误：'+data.error)
				return;
			}
			alert("修改成功")
		})
	})
});

function getTime(){
	var route = "admin/getTime"
	var args = null
	ajaxPost(route,args,function(data){
		$("#startYear").val(data.timmer[0].startYear)
		$("#startMonth").val(data.timmer[0].startMonth)
		$("#startDay").val(data.timmer[0].startDay)
		$("#endYear").val(data.timmer[0].endYear);
		$("#endMonth").val(data.timmer[0].endMonth);
		$("#endDay").val(data.timmer[0].endDay)
		
	})
}