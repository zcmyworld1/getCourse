$(document).ready(function() {
	var pageCount;
	var currentPage = 1;
	getFeedbackCount()
	$("#userMangeUserName").click(function() {
		$("#userManageSelect").slideToggle(1)
	})
	$("#logout").click(function() {
		$("#userManageSelect").slideUp(1)
		alert('退出登录')
	})
	$("#searchEnsure").click(function() {
		getFeedbackCount()
	})
	$(document).on('click', '.feedback_idDue', function() {
		var self = this;
		var feedbackId = $(this).parent().find('.feedbackId').val()
		var route = "feedback/updateIsDue";
		var args = {
			feedbackId: feedbackId
		}
		ajaxPost(route, args, function(data) {
			if (data.error) {
				alert('系统错误:' + data.error)
			}
			$(self).css('background-color', 'red')
			$(self).html('已完成')
		})
	})
	$("#nextPage").click(function() {
		currentPage = parseInt($("#currentPage").html())
		if (currentPage == pageCount) {
			alert('已经是最后一页')
			return;
		}
		currentPage++;
		$("#currentPage").html(currentPage)
		getFeedbackList(currentPage)
	})
	$("#prevPage").click(function() {
		currentPage = parseInt($("#currentPage").html())
		if (currentPage == 1) {
			alert('已经是首页')
			return;
		}
		currentPage--;
		$("#currentPage").html(currentPage)
		getFeedbackList(currentPage)
	})
});

function getFeedbackCount() {
	var key = $("#searcheCourse").val()
	var route = "feedback/getFeedbackCount";
	var args = {
		key: key
	};
	ajaxPost(route, args, function(data) {
		pageCount = data.feedbackCount[0].feedbackCount;
		pageCount = parseInt(data.feedbackCount[0].feedbackCount / 5) + 1;
		$("#pageCount").html(pageCount);
		getFeedbackList(1)
	})
}

function getFeedbackList(currentPage) {
	var key = $("#searcheCourse").val()
	$(".feedback_ele").remove()
	var route = "feedback/getFeedbackList"
	var args = {
		currentPage: currentPage,
		pageSize: 5,
		key: key
	}
	ajaxPost(route, args, function(data) {
		for (var i in data.feedbackList) {
			var d = 0;
			if (data.feedbackList[i].isDue == 0) {
				d = ' class="feedback_idDue">' + '未完成'
			}
			if (data.feedbackList[i].isDue == 1) {
				d = ' class="feedback_idDue" style="background:red">' + '已完成'
			}
			var html = '<div class="feedback_ele">' +
				'<input type="hidden" value="' + data.feedbackList[i].feedbackId + '" class="feedbackId">' +
				'<div class="feedback_userName">' + data.feedbackList[i].userName + '</div>' +
				'<div class="feedback_phoneNum">' + data.feedbackList[i].phoneNum + '</div>' +
				'<div class="feedback_time">' + data.feedbackList[i].feedbackTime + '</div>' +
				'<div class="feedback_content">' + data.feedbackList[i].content +
				'</div>' +
				'<div' + d + '</div>' +
				'<hr style="clear:both;">' + '</div>'
			$("#cl_content").append(html)
		}
		if (data.feedbackList.length == 0) {
			$("#cl_content").append('<div class="feedback_ele">查无消息</div>')
		}
	})
}