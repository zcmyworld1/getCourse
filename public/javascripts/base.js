var ip = window.location.hostname;
var port = "3000";

function ajaxPost(route, args, callback) {
	var url = "http://" + ip + ":" + port + "/" + route;
	$.ajax({
		url: url,
		type: "post",
		data: args,
		datatype: "json",
		error: function() {
			// alert("系统错误");
		},
		success: function(data) {
			callback(data);
		}
	});
};

function hint(data, callback) {
	$("#hintBox").show();
	$("#hint_title").html(data.title);
	$("#hint_contentBox").html(data.content);
	$("#hint_ensure").click(function() {
		$("#hintBox").hide();
		callback();
	});
	$("#hint_quit").click(function() {
		$("#hintBox").hide();
	})
}

$(document).on('click', '#nav_selectCourse', function() {
	window.location.href = '/student';
})
$(document).on('click', '#nav_getScore', function() {
	window.location.href = '/getScore';
})

$(document).on('click','#nav_courseList_teacher',function(){
	window.location.href="/teacher"
})
$(document).on('click','#nav_studentList_teacher',function(){
	window.location.href="/teacher_student"
})


$(document).on('click','#nav_courseList',function(){
	window.location.href="/admin"
})
$(document).on('click','#nav_studentList',function(){
	window.location.href = "/admin_student"
})
$(document).on('click','#nav_commentList',function(){
	window.location.href = "/admin_comment";
})
$(document).on('click','#nav_feedbackList',function(){
	window.location.href = "/admin_feedback";
})
$(document).on('click','#nav_system',function(){
	window.location.href = "/admin_system";
})
