$(document).ready(function() {
	$('#userMangeUserName').html($.cookie('userName'))
	var pageCount;
	var currentPage = 1;
	var haveSelectCount = 0;
	var isHaveSelect = false;

	var commentPageCount;
	var currentCommentPage = 1;
	// getCourseCount();
	getCourseList(1);
	getInitCurrentPage();
	getHaveSelectCourse();
	getHaveCourse();
	$("#userMangeUserName").click(function(){
		$("#userManageSelect").slideToggle(1)
	})
	$("#feedback").click(function(){
		$("#userManageSelect").slideUp(1)
		$("#feedbackBox").show();
	})
	$("#feedback_close").click(function(){
		$("#feedbackBox").hide();	
	})
	$("#feedback_ensure").click(function(){
		var phoneNum = $("#feedback_phone").val()
		var content = $("#feebackContent textarea").val()
		if(phoneNum.length == 0 ){
			alert('手机号码不能为空')
			return;
		}
		if(content.length == 0){
			alert('反馈内容不能为空')
			return;
		}
		var route = 'feedback/feedbackAdd'
		var args = {
			phoneNum:phoneNum,
			content:content
		}
		ajaxPost(route,args,function(data){
			if(data.error){
				alert('系统错误：'+data.error)
				return;
			}
			alert('反馈成功，稍后会有管理员向你联系')
			$("#feedbackBox").hide();		
		})
	})
	$("#logout").click(function(){
		$("#userManageSelect").slideUp(1)
		alert('退出登录')
	})
	$(document).on('click', '.courseName', function() {
		var courseId = $(this).parent().find('.courseId').val();
		var courseName = $(this).parent().find('.courseName').text()
		$("#comment_courseId").val(courseId)
		$("#comment_courseName").val(courseName)
		$("#commentBox").show();
		getCommentList(courseId)
	})
	$(document).on('click', '#commentSend_ensure', function() {
		var content = $("#commentSend textarea").val()
		var courseId = $("#comment_courseId").val()
		var courseName = $("#comment_courseName").val()
		var route = "comment/commentAdd";
		var args = {
			content: content,
			courseId: courseId,
			courseName:courseName
		}
		ajaxPost(route, args, function(data) {
			alert('添加成功')
			getCommentList(courseId)
		})
	})
	$(document).on('click', '#commentSend_close', function() {
		$("#commentBox").hide()
	})
	$(document).on('click', '.operationSelect', function() {
		haveSelectCount = 0;
		isHaveSelect = false;
		var courseId = $(this).parent().parent().find(".courseId").val();
		$(".haveSelectCourse").each(function() {
			if ($(this).find('.courseId').val() == courseId) {
				alert("已经选择了该课程");
				isHaveSelect = true;
			}
			haveSelectCount++;
		});
		if (isHaveSelect) {
			haveSelectCount = 0;
			return;
		}
		if (haveSelectCount > 2) {
			alert("课程已满不能再选");
			haveSelectCount = 0;
			return;
		}
		var courseName = $(this).parent().parent().find(".courseName").val();
		var html = "<div class='haveSelectCourse'><input type='hidden' class='courseId' value='" + courseId + "'><span class='courseName'>" + courseName + "</span><img src='images/sysImg/close.png'></div>";

		$("#cr_content_top").append(html);
		haveSelectCount = 0;
		//将已有课程存到cookie
		var courseArrId = new Array();
		var courseArrName = new Array();
		var i = 0;
		$(".haveSelectCourse").each(function() {
			courseArrId[i] = parseInt($(this).find('.courseId').val());
			courseArrName[i] = $(this).find('.courseName').html();
			i++;
		});
		$.cookie('haveSelectCourseId', courseArrId, {
			expires: 3650
		});
		$.cookie('haveSelectCourseName', courseArrName, {
			expires: 3650
		});
	})
	$(document).on('click', '#cr_content_ensure', function() {
		var courseArr = new Array();
		var i = 0;
		$(".haveSelectCourse").each(function() {
			courseArr[i] = parseInt($(this).find('.courseId').val());
			i++;
		});
		var route = 'course/ensureCoures';
		var args = {
			courseArr: courseArr
		}
		ajaxPost(route, args, function(data) {
			if (data.error == 1) {
				alert('现在不是选课的时间');
			}
			if (data.error == 2) {
				alert('过去已经选过这一门课了');
			}
			alert('选课成功！')
		})
	})
	$(document).on('click', '.haveSelectCourse img', function() {
		$(this).parent().remove();
		var courseArrId = new Array();
		var courseArrName = new Array();
		var i = 0;
		$(".haveSelectCourse").each(function() {
			courseArrId[i] = parseInt($(this).find('.courseId').val());
			courseArrName[i] = $(this).find('.courseName').html();
			i++;
		});
		$.cookie('haveSelectCourseId', courseArrId, {
			expires: 3650
		});
		$.cookie('haveSelectCourseName', courseArrName, {
			expires: 3650
		});
	})
	$(document).on('click', '.haveCourse img', function() {
		var hintargs = {
			title: '提示',
			content: '确定退选这一门课吗？'
		}
		var currentCourse = $(this);
		var courseId = $(this).parent().find('.courseId').val();
		hint(hintargs, function() {
			var route = 'course/quitCourse';
			var args = {
				courseId: courseId
			}
			ajaxPost(route, args, function(data) {
				if (data.error == 1) {
					alert('选课日期已过，不能再选修改');
					return;
				}
				currentCourse.parent().remove();
				alert('成功退课');
			})
		})
	})
	$(document).on('click', '#prevPage', function() {
		var currentPage = parseInt($("#currentPage").html());
		if (currentPage == 1) {
			alert("已经是首页");
			return;
		}
		var currentPage = parseInt($("#currentPage").html()) - 1;
		$("#currentPage").html(currentPage);
		getCourseList(currentPage);
	})
	$(document).on('click', '#nextPage', function() {
		var currentPage = parseInt($("#currentPage").html());
		var pageCount = parseInt($("#pageCount").html());
		if (currentPage == pageCount) {
			alert("已经是最后一页");
			return;
		}
		var currentPage = parseInt($("#currentPage").html()) + 1;
		$("#currentPage").html(currentPage);
		getCourseList(currentPage);
	})
	$(document).on('click', '#searchEnsure', function() {
		var courseName = $("#searcheCourse").val();
		getCourseList(1, courseName);
	})
});

function getSearchCourse(currentPage, courseName) {
	$('.courseData').remove();
	var pageSize = 14;
	var route = "course/searchCourse";
	var args = {
		courseName: courseName
	}
	ajaxPost(route, args, function(data) {
		for (var i in data.courseList) {
			if (data.courseList[i].courseType == 1) {
				data.courseList[i].courseType = "限定性艺术类";
			}
			if (data.courseList[i].courseType == 2) {
				data.courseList[i].courseType = "非限定性艺术类";
			}
			var html =
				"<tr class='courseData'>" +
				"<input type='hidden' class='courseId' value='" + data.courseList[i].courseId + "'>" +
				"<td class='courseName'>" + data.courseList[i].courseName + "</td>" +
				"<td>" + data.courseList[i].teacherName + "</td>" +
				"<td>" + data.courseList[i].courseTime + "</td>" +
				"<td>" + data.courseList[i].credit + "</td>" +
				"<td>" + data.courseList[i].courseType + "</td>" +
				"<td>" + data.courseList[i].permitNum + "</td>" +
				"<td>" + data.courseList[i].haveNum+ "</td>" +
				"<td>" +
				"<div class='operationSelect'>选择</div>" +
				"</td>" +
				"</tr>";
			$("#courseTable").append(html);
		}
	})
}

function getCourseList(currentPage, courseName) {
	$('.courseData').remove();
	var pageSize = 14;
	var route = "course/getCourseList";
	var args = {
		currentPage: currentPage,
		pageSize: pageSize,
		courseName: courseName
	}
	ajaxPost(route, args, function(data) {
		pageCount = parseInt(data.courseList[0].courseCount / 14) + 1;
		$("#pageCount").html(pageCount);
		for (var i in data.courseList) {
			if (data.courseList[i].courseType == 1) {
				data.courseList[i].courseType = "限定性艺术类";
			}
			if (data.courseList[i].courseType == 2) {
				data.courseList[i].courseType = "非限定性艺术类";
			}
			var html =
				"<tr class='courseData'>" +
				"<input type='hidden' class='courseId' value='" + data.courseList[i].courseId + "'>" +
				"<input type='hidden' class='courseName' value='" + data.courseList[i].courseName + "'>" +
				"<td class='courseName'>" + data.courseList[i].courseName + "</td>" +
				"<td>" + data.courseList[i].teacherName + "</td>" +
				"<td>" + data.courseList[i].courseTime + "</td>" +
				"<td>" + data.courseList[i].credit + "</td>" +
				"<td>" + data.courseList[i].courseType + "</td>" +
				"<td>" + data.courseList[i].permitNum + "</td>" +
				"<td>" + data.courseList[i].haveNum+"</td>" +
				"<td>" +
				"<div class='operationSelect'>选择</div>" +
				"</td>" +
				"</tr>";
			$("#courseTable").append(html);
		}
	})
}

function getInitCurrentPage() {
	$("#currentPage").html(1);
}

function getCourseCount() {
	var pageSize = 14;
	var route = "course/getCourseCount";
	var args = null;
	ajaxPost(route, args, function(data) {
		pageCount = parseInt(data.courseCount[0].courseCount / 14) + 1;
		$("#pageCount").html(pageCount);
		getCourseList(1);
	})
}

//预选课程
function getHaveSelectCourse() {
	if (!$.cookie('haveSelectCourseName') || !$.cookie('haveSelectCourseId')) {
		return; //如果cookie没有存储则不执行此方法
	}
	var str = $.cookie('haveSelectCourseName');
	var strlen = str.length;
	var courseCount = 0;
	var cutCount = 0;
	obj = str.match(/,/g)
	if (obj != null) {
		cutCount = obj.length;
	}
	var startCut = 0;
	var classArr = [];
	for (var i = 0; i < cutCount + 1; i++) {
		var strcut = str.indexOf(',');
		var strclass = str.substring(startCut, strcut);
		str = str.substring(strcut + 1, strlen);
		if (strcut == -1) {
			classArr.push(str);
		} else {
			classArr.push(strclass);
		}
	}
	var idstr = $.cookie('haveSelectCourseId');
	var idstrlen = idstr.length;
	var idcourseCount = 0;
	var idcutCount = 0;
	obj = idstr.match(/,/g)
	if (obj != null) {
		idcutCount = obj.length;
	}
	var idstartCut = 0;
	var idclassArr = [];
	for (var i = 0; i < idcutCount + 1; i++) {
		var idstrcut = idstr.indexOf(',');
		var idstrclass = idstr.substring(idstartCut, idstrcut);
		idstr = idstr.substring(idstrcut + 1, idstrlen);
		if (idstrcut == -1) {
			idclassArr.push(idstr);
		} else {
			idclassArr.push(idstrclass);
		}
	}
	for (var i in classArr) {
		var html = "<div class='haveSelectCourse'><input type='hidden' class='courseId' value='" + idclassArr[i] + "'><span class='courseName'>" + classArr[i] + "</span><img src='images/sysImg/close.png'></div>";
		$("#cr_content_top").append(html);
	}
}

//已经选课程
function getHaveCourse() {
	var route = "course/getHaveSelectCourse";
	var args = null;
	ajaxPost(route, args, function(data) {
		for (var i in data.haveSelectCourse) {
			var html = "<div class='haveCourse'><input type='hidden' class='courseId' value='" + data.haveSelectCourse[i].courseId + "'><span>" + data.haveSelectCourse[i].courseName + "</span><img src='images/sysImg/close.png'></div>";
			$("#crb_content_top").append(html);
		}
	})
}

function getCommentList(courseId) {
	$(".comment_ele").remove()
	var route = "comment/getCommentList";
	var args = {
		courseId: courseId
	}
	ajaxPost(route, args, function(data) {
		for (var i in data.commentList) {
			var t = new Date(data.commentList[i].commentTime);
			var y = t.getFullYear() +'-'+(t.getMonth()+1)+'-'+(t.getDate())+" " +(t.getHours()+1)+":"+(t.getMinutes())
			var html = '<div class="comment_ele">' +
				'<div>' +
				'<span class="commentName">'+data.commentList[i].trueName+'</span>' +
				'<span class="commentTime">'+y+'</span>' +
				'</div>' +
				'<div class="commentContent">'+data.commentList[i].content+'</div>' +
				'</div>';
			$("#commentList").append(html)
		}
		if(data.commentList.length == 0){
			$("#commentList").append('<div class="comment_ele">没有任何评论</div>')	
		}
	})
}