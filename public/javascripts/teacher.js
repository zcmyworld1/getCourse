var currentPage = 1;
var pageCount = 0;
var courseType_g = 0;
var addOrEdit = -1; //1为添加2为编辑
$(document).ready(function() {
	$(document).on('click', '.courseName', function() {
		var courseId = $(this).parent().find('.courseId').val();
		var courseName = $(this).parent().find('.courseName').text()
		$("#comment_courseId").val(courseId)
		$("#comment_courseName").val(courseName)
		$("#commentBox").show();
		getCommentList(courseId)
	})
	$("#userMangeUserName").click(function(){
		$("#userManageSelect").slideToggle(1)
	})
	$("#logout").click(function(){
		$("#userManageSelect").slideUp(1)
		alert('退出登录')
	})
	$("#feedback").click(function(){
		$("#userManageSelect").slideUp(1)
		$("#feedbackBox").show();
	})
	$("#feedback_close").click(function(){
		$("#feedbackBox").hide();	
	})
	$("#feedback_ensure").click(function(){
		var phoneNum = $("#feedback_phone").val()
		var content = $("#feebackContent textarea").val()
		if(phoneNum.length == 0 ){
			alert('手机号码不能为空')
			return;
		}
		if(content.length == 0){
			alert('反馈内容不能为空')
			return;
		}
		var route = 'feedback/feedbackAdd'
		var args = {
			phoneNum:phoneNum,
			content:content
		}
		ajaxPost(route,args,function(data){
			if(data.error){
				alert('系统错误：'+data.error)
				return;
			}
			alert('反馈成功，稍后会有管理员向你联系')
			$("#feedbackBox").hide();		
		})
	})
	$(document).on('click', '#commentSend_ensure', function() {
		var content = $("#commentSend textarea").val()
		var courseId = $("#comment_courseId").val()
		var courseName = $("#comment_courseName").val();
		var route = "comment/commentAdd";
		var args = {
			content: content,
			courseId: courseId,
			courseName:courseName
		}
		ajaxPost(route, args, function(data) {
			alert('添加成功')
			getCommentList(courseId)
		})
	})
	$(document).on('click', '#commentSend_close', function() {
		$("#commentBox").hide()
	})
	$(document).on('click',"#operationSelect_del",function(){
		var courseId = $(this).parent().parent().find('.courseId').val();
		var route = "teacher/deleteCourse"
		var args = {
			courseId:courseId
		}
		ajaxPost(route,args,function(data){
			if(data.error){
				return
			}
			alert("删除成功")
			getCourseList(currentPage)
		})
	})
	$("#nav_courseList").click(function(){
		window.location.href="teacher";
	})
	$("#nav_studentList").click(function(){
		alert(2)
	})
	$(document).on('click', '#operationSelect_edit', function() {
		var courseId = $(this).parent().parent().find('.courseId').val();
		addOrEdit = 2;
		$("#addCourseBox").show();
		var route = "teacher/getSingleCourse";
		var args = {
			courseId:courseId
		};
		$("#addCourseBox_courseId").val(courseId)
		ajaxPost(route, args, function(data) {
			$("#addCourseBox_courseName input").val(data.course[0].courseName);
			$("#addCourseBox_courseTime input").val(data.course[0].courseTime);
			$("#addCourseBox_credit input").val(data.course[0].credit);
			$("#addCourseBox_permitNum input").val(data.course[0].permitNum);
			$("#addCourseBox_courseRoom input").val(data.course[0].courseRoom);
			if (data.course[0].courseType == 1) {
				//限定性艺术类
				$("#ct_left").text("限定性艺术类");
				courseType_g = 1;
			}
			if (data.course[0].courseType == 2) {
				courseType_g = 2;
				$("#ct_left").text("非限定性艺术类");
			}
			$("#addCourseBox_courseText textarea").val(data.course[0].courseText);
		})
	})
	$("#addCourseBox_close").click(function() {
		$("#addCourseBox").hide()
	})
	$("#addCourse").click(function() {
		addOrEdit = 1;
		$("#addCourseBox").show();
	})
	$("#ct_left").click(function() {
		$("#ct_right_bottom").fadeIn();
		$("#ct_right_top").fadeIn();
	})
	$("#ct_right_bottom").click(function() {
		//非限定性艺术类
		$("#ct_right_bottom").fadeOut();
		$("#ct_right_top").fadeOut();
		$("#ct_left").text($(this).text());
		courseType_g = 2;
	})
	$("#ct_right_top").click(function() {
		//限定性艺术类
		$("#ct_right_bottom").fadeOut();
		$("#ct_right_top").fadeOut();
		$("#ct_left").text($(this).text());
		courseType_g = 1;
	})
	$("#searchEnsure").click(function() {
		getCourseList(currentPage)
	})
	$("#addCourseBox_ensure").click(function() {
		var courseName = $("#addCourseBox_courseName input").val();
		var courseTime = $("#addCourseBox_courseTime input").val();
		var credit = $("#addCourseBox_credit input").val();
		var permitNum = $("#addCourseBox_permitNum input").val();
		var courseRoom = $("#addCourseBox_courseRoom input").val();
		var courseType = courseType_g;
		var courseText = $("#addCourseBox_courseText textarea").val();
		var courseId = $("#addCourseBox_courseId").val()
		var args = {
			courseId:courseId,
			courseName: courseName,
			courseTime: courseTime,
			credit: credit,
			permitNum: permitNum,
			courseRoom: courseRoom,
			courseType: courseType,
			courseText: courseText
		}
		if (addOrEdit == 1) {
			var route = "teacher/courseAdd";
			ajaxPost(route, args, function(data) {
				alert('添加成功');
				$("#addCourseBox").hide();
				getCourseList(currentPage)
			})
		}
		if(addOrEdit == 2){
			var route = "teacher/courseEdit";
			ajaxPost(route,args,function(data){
				alert('编辑成功');
				$("#addCourseBox").hide();
				getCourseList(currentPage)
			})
		}
		return;

	})
	getCourseListCount();
});


function getCourseListCount() {
	var route = 'teacher/getCourseListCount';
	var key = $("#searcheCourse").val();
	var args = {
		key: key
	};
	ajaxPost(route, args, function(data) {
		pageCount = parseInt(data.courseListCount[0].courseCount / 14) + 1;
		$("#pageCount").text(pageCount)
	})
	getCourseList(currentPage)
}

function getCourseList(currentPage) {
	$(".courseData").remove();
	var pageSize = 14;
	var route = 'teacher/getCourseList'
	var key = $("#searcheCourse").val();
	var args = {
		currentPage: currentPage,
		pageSize: pageSize,
		key: key
	};
	ajaxPost(route, args, function(data) {
		for (var i in data.courseList) {
			// if()
			var tt = data.courseList[i].insertTime			
			// var t = tt.getYear()
			// console.log(tt)
			var t = new Date(tt)
			t = t.getFullYear() + "-" + (t.getMonth()+1)
			if (data.courseList[i].courseType == 1) {
				data.courseList[i].courseType = "限定性艺术类";
			}
			if (data.courseList[i].courseType == 2) {
				data.courseList[i].courseType = "非限定性艺术类";
			}
			var html =
				"<tr class='courseData'>" +
				"<input type='hidden' class='courseId' value='" + data.courseList[i].courseId + "'>" +
				"<td class='courseName'>" + data.courseList[i].courseName + "</td>" +
				"<td>" + data.courseList[i].teacherName + "</td>" +
				"<td>" + data.courseList[i].courseTime + "</td>" +
				"<td>" + data.courseList[i].credit + "</td>" +
				"<td>" + data.courseList[i].courseType + "</td>" +
				"<td>" + t + "</td>" +
				"<td>" + data.courseList[i].permitNum + "</td>" +
				"<td>159</td>" +
				"<td>" +
				"<div class='operationSelect' id='operationSelect_edit'>编辑</div>" +
				"<div class='operationSelect' id='operationSelect_del'>删除</div>" +
				"</td>" +
				"</tr>";
			$("#courseTable").append(html);
		}
		if (data.courseList.length == 0) {
			var html = "<div class='courseData'>没有数据</div>";
			$("#courseTable").append(html);
		}
	})
}

function getCommentList(courseId) {
	$(".comment_ele").remove()
	var route = "comment/getCommentList";
	var args = {
		courseId: courseId
	}
	ajaxPost(route, args, function(data) {
		for (var i in data.commentList) {
			var t = new Date(data.commentList[i].commentTime);
			var y = t.getFullYear() +'-'+(t.getMonth()+1)+'-'+(t.getDate())+" " +(t.getHours()+1)+":"+(t.getMinutes())
			var html = '<div class="comment_ele">' +
				'<div>' +
				'<span class="commentName">'+data.commentList[i].trueName+'</span>' +
				'<span class="commentTime">'+y+'</span>' +
				'</div>' +
				'<div class="commentContent">'+data.commentList[i].content+'</div>' +
				'</div>';
			$("#commentList").append(html)
		}
		if(data.commentList.length == 0){
			$("#commentList").append('<div class="comment_ele">没有任何评论</div>')	
		}
	})
}