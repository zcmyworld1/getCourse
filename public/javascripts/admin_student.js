var courseType_g = 0;
$(document).ready(function() {
	var pageCount;
	var currentPage = 1;
	getStudentCount()
	$("#userMangeUserName").click(function() {
		$("#userManageSelect").slideToggle(1)
	})
	$("#logout").click(function() {
		$("#userManageSelect").slideUp(1)
		alert('退出登录')
	})
	$("#searchEnsure").click(function() {
		getStudentCount()
	})
	
	$("#nextPage").click(function() {
		currentPage = parseInt($("#currentPage").html())
		var pageCount = parseInt($("#pageCount").html())
		if (currentPage == pageCount) {
			alert('已经是最后一页')
			return;
		}
		currentPage++;
		$("#currentPage").html(currentPage)
		getStudentCount(currentPage)
	})
	$("#prevPage").click(function() {
		currentPage = parseInt($("#currentPage").html())
		if (currentPage == 1) {
			alert('已经是首页')
			return;
		}
		currentPage--;
		$("#currentPage").html(currentPage)
		getStudentCount(currentPage)
	})
	
});

function getStudentCount() {
	var route = "admin/getStudentCount";
	var key = $("#searcheCourse").val()
	var args = {
		key: key
	}
	ajaxPost(route, args, function(data) {
		pageCount = data.studentCount[0].studentCount;
		pageCount = parseInt(data.studentCount[0].studentCount / 14) + 1;
		$("#pageCount").html(pageCount)
		currentPage = parseInt($("#currentPage").html())
		getCourseList(currentPage)
	})
}

function getCourseList(currentPage) {
	$(".student_ele").remove();
	var route = "admin/getStudentList";
	var key = $("#searcheCourse").val()
	var pageSize = 14;
	var args = {
		currentPage: currentPage,
		pageSize: pageSize,
		key: key
	}
	ajaxPost(route, args, function(data) {
		for (var i in data.studentList) {
			if(data.studentList[i].sex == 1){
				data.studentList[i].sex = "男"
			}else{
				data.studentList[i].sex = "女"
			}
			var html = '<tr>'+
			'<td style="height:40px;line-height: 40px;width:200px;" class="student_ele">'+data.studentList[i].trueName+'</td>'+
			'<td style="height:40px;line-height: 40px;width:100px;" class="student_ele">'+data.studentList[i].sex+'</td>'+
			'<td style="height:40px;line-height: 40px;width:200px;" class="student_ele">'+data.studentList[i].stuNum+'</td>'+
			'<td style="height:40px;line-height: 40px;width:300px;" class="student_ele">'+data.studentList[i].major+'</td>'+
			'<td style="height:40px;line-height: 40px;width:200px;" class="student_ele">'+data.studentList[i].grade+'</td>'+
			'</tr>'
			$("#courseTable").append(html)
		}
		if(data.studentList.length == 0){
			var html = "<div class='student_ele'>没有信息</div>"
			$("#courseTable").append(html)
		}
	})
}