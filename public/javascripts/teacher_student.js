var currentPage = 1;
var pageCount = 0;
var courseType_g = 0;
var addOrEdit = -1; //1为添加2为编辑
$(document).ready(function() {
	$(document).on('click', '.courseName', function() {
		var courseId = $(this).parent().find('.courseId').val();
		var route = "admin/getStudentListByCourse"
		var args = {
			courseId: courseId
		}
		ajaxPost(route, args, function(data) {
			$("#studentListBox").show()
			for (var i in data.studentList) {
				var html = '<tr>' +
					'<input type="hidden" class="userId" value="'+data.studentList[i].userId+'">'+
					'<td style="width:150px;text-align:center">'+data.studentList[i].trueName+'</td>' +
					'<td style="width:200px;text-align:center">'+data.studentList[i].stuNum+'</td>' +
					'<td style="width:60px;text-align:center">'+data.studentList[i].sex+'</td>' +
					'<td style="width:50px;text-align:center">'+data.studentList[i].grade+'</td>' +
					'<td style="width:250px;text-align:center">'+data.studentList[i].major+'</td>' +
					'<td style="width:100px;text-align:center">'+
						'<input style="text-align:center" type="text" class="score" value="'+data.studentList[i].score+'">'+
					'</td>' +
					'</tr>'
				$("#stuudentList").append(html)
			}
		})
	})
	$(document).on('change','.score',function(){
		var userId = $(this).parent().parent().find('.userId').val()
		var score = $(this).val()
	})
	$("#userMangeUserName").click(function() {
		$("#userManageSelect").slideToggle(1)
	})
	$("#logout").click(function() {
		$("#userManageSelect").slideUp(1)
		alert('退出登录')
	})
	$("#feedback").click(function() {
		$("#userManageSelect").slideUp(1)
		$("#feedbackBox").show();
	})
	$("#feedback_close").click(function() {
		$("#feedbackBox").hide();
	})
	$("#feedback_ensure").click(function() {
		var phoneNum = $("#feedback_phone").val()
		var content = $("#feebackContent textarea").val()
		if (phoneNum.length == 0) {
			alert('手机号码不能为空')
			return;
		}
		if (content.length == 0) {
			alert('反馈内容不能为空')
			return;
		}
		var route = 'feedback/feedbackAdd'
		var args = {
			phoneNum: phoneNum,
			content: content
		}
		ajaxPost(route, args, function(data) {
			if (data.error) {
				alert('系统错误：' + data.error)
				return;
			}
			alert('反馈成功，稍后会有管理员向你联系')
			$("#feedbackBox").hide();
		})
	})
	$(document).on('click', '#commentSend_ensure', function() {
		var content = $("#commentSend textarea").val()
		var courseId = $("#comment_courseId").val()
		var courseName = $("#comment_courseName").val();
		var route = "comment/commentAdd";
		var args = {
			content: content,
			courseId: courseId,
			courseName: courseName
		}
		ajaxPost(route, args, function(data) {
			alert('添加成功')
			getCommentList(courseId)
		})
	})
	$(document).on('click', '#commentSend_close', function() {
		$("#commentBox").hide()
	})
	$(document).on('click', "#operationSelect_del", function() {
		var courseId = $(this).parent().parent().find('.courseId').val();
		var route = "teacher/deleteCourse"
		var args = {
			courseId: courseId
		}
		ajaxPost(route, args, function(data) {
			if (data.error) {
				return
			}
			alert("删除成功")
			getCourseList(currentPage)
		})
	})
	$("#nav_courseList").click(function() {
		window.location.href = "teacher";
	})
	$("#nav_studentList").click(function() {
		alert(2)
	})
	$(document).on('click', '#operationSelect_edit', function() {
		var courseId = $(this).parent().parent().find('.courseId').val();
		addOrEdit = 2;
		$("#addCourseBox").show();
		var route = "teacher/getSingleCourse";
		var args = {
			courseId: courseId
		};
		$("#addCourseBox_courseId").val(courseId)
		ajaxPost(route, args, function(data) {
			$("#addCourseBox_courseName input").val(data.course[0].courseName);
			$("#addCourseBox_courseTime input").val(data.course[0].courseTime);
			$("#addCourseBox_credit input").val(data.course[0].credit);
			$("#addCourseBox_permitNum input").val(data.course[0].permitNum);
			$("#addCourseBox_courseRoom input").val(data.course[0].courseRoom);
			if (data.course[0].courseType == 1) {
				//限定性艺术类
				$("#ct_left").text("限定性艺术类");
				courseType_g = 1;
			}
			if (data.course[0].courseType == 2) {
				courseType_g = 2;
				$("#ct_left").text("非限定性艺术类");
			}
			$("#addCourseBox_courseText textarea").val(data.course[0].courseText);
		})
	})
	$("#addCourseBox_close").click(function() {
		$("#addCourseBox").hide()
	})
	$("#addCourse").click(function() {
		addOrEdit = 1;
		$("#addCourseBox").show();
	})
	$("#ct_left").click(function() {
		$("#ct_right_bottom").fadeIn();
		$("#ct_right_top").fadeIn();
	})
	$("#ct_right_bottom").click(function() {
		//非限定性艺术类
		$("#ct_right_bottom").fadeOut();
		$("#ct_right_top").fadeOut();
		$("#ct_left").text($(this).text());
		courseType_g = 2;
	})
	$("#ct_right_top").click(function() {
		//限定性艺术类
		$("#ct_right_bottom").fadeOut();
		$("#ct_right_top").fadeOut();
		$("#ct_left").text($(this).text());
		courseType_g = 1;
	})
	$("#searchEnsure").click(function() {
		getCourseList(currentPage)
	})
	$("#addCourseBox_ensure").click(function() {
		var courseName = $("#addCourseBox_courseName input").val();
		var courseTime = $("#addCourseBox_courseTime input").val();
		var credit = $("#addCourseBox_credit input").val();
		var permitNum = $("#addCourseBox_permitNum input").val();
		var courseRoom = $("#addCourseBox_courseRoom input").val();
		var courseType = courseType_g;
		var courseText = $("#addCourseBox_courseText textarea").val();
		var courseId = $("#addCourseBox_courseId").val()
		var args = {
			courseId: courseId,
			courseName: courseName,
			courseTime: courseTime,
			credit: credit,
			permitNum: permitNum,
			courseRoom: courseRoom,
			courseType: courseType,
			courseText: courseText
		}
		if (addOrEdit == 1) {
			var route = "teacher/courseAdd";
			ajaxPost(route, args, function(data) {
				alert('添加成功');
				$("#addCourseBox").hide();
				getCourseList(currentPage)
			})
		}
		if (addOrEdit == 2) {
			var route = "teacher/courseEdit";
			ajaxPost(route, args, function(data) {
				alert('编辑成功');
				$("#addCourseBox").hide();
				getCourseList(currentPage)
			})
		}
		return;

	})
	getCourseListCount();
});


function getCourseListCount() {
	var route = 'teacher/getCourseListCount';
	var key = $("#searcheCourse").val();
	var args = {
		key: key
	};
	ajaxPost(route, args, function(data) {
		pageCount = parseInt(data.courseListCount[0].courseCount / 14) + 1;
		$("#pageCount").text(pageCount)
	})
	getCourseList(currentPage)
}

function getCourseList(currentPage) {
	$(".courseData").remove();
	var pageSize = 14;
	var route = 'teacher/getCourseList'
	var key = $("#searcheCourse").val();
	var args = {
		currentPage: currentPage,
		pageSize: pageSize,
		key: key
	};
	ajaxPost(route, args, function(data) {
		for (var i in data.courseList) {

			var html =
				"<tr class='courseData'>" +
				"<input type='hidden' class='courseId' value='" + data.courseList[i].courseId + "'>" +
				"<td class='courseName'>" + data.courseList[i].courseName + "</td>" +

				"</tr>";
			$("#courseTable").append(html);
		}
		if (data.courseList.length == 0) {
			var html = "<div class='courseData'>没有数据</div>";
			$("#courseTable").append(html);
		}
	})
}