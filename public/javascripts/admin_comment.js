$(document).ready(function() {
	var pageCount;
	var currentPage = 1;
	getCommentCount()
	$("#userMangeUserName").click(function() {
		$("#userManageSelect").slideToggle(1)
	})
	$("#logout").click(function() {
		$("#userManageSelect").slideUp(1)
		alert('退出登录')
	})
	$("#searchEnsure").click(function() {
		getCommentCount()
	})

	$(document).on('click','.comment_del',function(){
		var commentId = $(this).parent().find('.commentId').val()
		var route = "admin/delComment"
		var args = {
			commentId:commentId
		}
		ajaxPost(route,args,function(data){
			if(data.error){
				alert('系统错误：'+error)
				return
			}
			alert('删除成功')
		})
		getCommentCount()
	})

	$("#nextPage").click(function() {
		currentPage = parseInt($("#currentPage").html())
		var pageCount = parseInt($("#pageCount").html())
		if (currentPage == pageCount) {
			alert('已经是最后一页')
			return;
		}
		currentPage++;
		$("#currentPage").html(currentPage)
		getCommentList(currentPage)
	})
	$("#prevPage").click(function() {
		currentPage = parseInt($("#currentPage").html())
		if (currentPage == 1) {
			alert('已经是首页')
			return;
		}
		currentPage--;
		$("#currentPage").html(currentPage)
		getCommentList(currentPage)
	})
});

function getCommentCount() {
	var key = $("#searcheCourse").val()
	var route = "admin/getCommentCount";
	var args = {
		key: key
	};
	ajaxPost(route, args, function(data) {
		console.log(data)
		pageCount = data.commentCount[0].commentCount;
		pageCount = parseInt(data.commentCount[0].commentCount / 5) + 1;
		$("#pageCount").html(pageCount);

		getCommentList(1)
	})
}

function getCommentList(currentPage) {
	var key = $("#searcheCourse").val()
	$(".comment_ele").remove()
	var route = "admin/getCommentList"
	var args = {
		currentPage: currentPage,
		pageSize: 5,
		key: key
	}
	ajaxPost(route, args, function(data) {
		for (var i in data.commentList) {
			var html = '<div class="comment_ele">' +
				'<input type="hidden" class="commentId" value="'+data.commentList[i].commentId+'">' +
				'<div class="comment_userName">'+data.commentList[i].userName+'</div>' +
				'<div class="comment_courseName">'+data.commentList[i].courseName+'</div>' +
				'<div class="comment_commentTime">'+data.commentList[i].commentTime+'</div>' +
				'<div class="comment_content">'+data.commentList[i].content+'</div>' +
				'<div class="comment_del">删除</div>' +
				'<hr style="clear:both;">' +
				'</div>'
			$("#cl_content").append(html);
		}
		if(data.commentList.length == 0){
			$("#cl_content").append('<div class="comment_ele">查无数据</div>')
		}
	})
}